from django.urls import include, path
from django.conf.urls import include
from .views import index
from django.conf import settings
from . import views

urlpatterns = [
    path('challenge/index.html', index, name='index'),
    path('challenge/', index, name='index'),
    path('challenge', index, name='index'),
    ]
