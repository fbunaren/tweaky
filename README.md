# Tweaky
## Created by Fransiscus Emmanuel Bunaren ( 1806173506 )

A simple social media site that is very similar to twitter

- Code Coverage

![100% Code Coverage](https://gitlab.com/fbunaren/tweaky/badges/master/coverage.svg)

- Pipeline

![Pipeline Success](https://gitlab.com/fbunaren/tweaky/badges/master/pipeline.svg)