from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase

#Story 7
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

#Model to be tested
from .models import Status

# Create your tests here.
class AppTest(TestCase):
    def test_index_response(self):
        response = Client().get('/index.html')
        self.assertEqual(response.status_code,200)
    
    def test_index_template(self):
        response = Client().get('/index.html')
        self.assertTemplateUsed(response,'index.html')

    def test_status_add(self):
        #Making new Status
        status = Status.objects.create(msg="Test Status")        
        #Check if status has been stored in database
        count_status = Status.objects.all().count()
        self.assertTrue(count_status > 0)

    def test_can_save_post_request(self):
        response = self.client.post('',data={'name':'Fransiscus','msg':'Tweaky'})
        #Check if status has been stored in database
        count_status = Status.objects.all().count()
        self.assertTrue(count_status > 0)        
        #Check if webpage is accessible
        self.assertEqual(response.status_code,200)
        #Check if the content is the same
        new_response = self.client.get('')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Tweaky',html_response)

#Story 7 Functional Test
class FunctionalTestTweaky(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTestTweaky, self).setUp()
        opt = Options()
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=opt)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()



