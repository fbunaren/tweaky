from django.test import TestCase
from django.test import Client
from django.test import *

# Create your tests here.
class AppTest(TestCase):
    def test_index_response(self):
        #Check HTTP Response
        response = Client().get('/challenge/index.html')
        self.assertEqual(response.status_code,200)

        #Check if NPM Template is well loaded
        self.assertTemplateUsed(response,'challenge.html')

        #Check if the NPM is Correct
        self.assertIn('1806173506',response.content.decode('utf8'))