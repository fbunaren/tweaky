from django.shortcuts import render
from .models import Status
from .forms import StatusForm

def index(request):
    response = {'form' : StatusForm()}
    if request.method == 'POST':
        status = Status.objects.create(msg=request.POST['msg'])
        status.save()
    #Load statuses
    response['statuses'] = Status.objects.all().values()
    return render(request,'index.html',response)
    